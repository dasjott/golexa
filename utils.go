package amzn

import (
	"io/ioutil"
	"math/rand"
	"path"
	"strings"
)

// GetSSML gets a template by its name
func GetSSML(name string, data SSMLData) string {
	theFile := path.Join(SSMLDirectory, name+".htm")
	dat, err := ioutil.ReadFile(theFile)
	if err == nil {
		str := string(dat)
		pos := strings.Index(str, "{")

		for ; pos > 0; pos = strings.Index(str, "{") {
			end := strings.Index(str, "}")
			str = str[:pos] + data[str[pos+1:end]] + str[end+1:]
		}
		return str
	}

	return ""
}

type SSMLData map[string]string

func RandString(list []string) string {
	len := len(list)
	choice := rand.Intn(len)
	return list[choice]
}

# AMAZIN' GOLEXA
A Server, structs, framework and a small template engine to quickly start writing intents for Alexa skills.<br>
Intended as one microservice for one skill.<br>
Easy to use and learn.

## Example
### main.go
```go
package main

import "gitlab.com/dasjott/golexa/amzn"

// specify the template ssml directory (used in OutputSSML2)
amzn.SSMLDirectory = "ssml"

// make alexa object
alexa := amzn.Alexa{}

// define a launch callback
alexa.OnLaunch(func(req *amzn.EchoRequest, resp *amzn.EchoResponse) {
	resp.OutputText("Welcome to my skill")
})

// add one intent "greetings", loading the template "first" with some variables and return that
alexa.OnIntent("greetings", func(req *amzn.EchoRequest, resp *amzn.EchoResponse) {
	resp.OutputSSML2("first", amzn.SSMLData{"NAME": "Hans", "AGE": "42"}).EndSession()
})

// run alexa server on port 80
alexa.Run(80)
```
It is possible to provide multiple functions to AddIntent or OnLaunch, to run multiple functions for one intent/launch call.<br>

### ./ssml/first.htm
```html
<speak>
	Hello, {NAME} <break time="900ms"/> You are {AGE} years old! Good luck.
</speak>
```
I chose htm extension because many good extensions work well on that.

## License
[MIT](LICENSE)

## TODO
- AudioPlayer request handling
- The template engine will propably grow a little more in the future.
- Documentation is coming

package amzn

import "encoding/json"

type EchoResponse struct {
	Version           string                 `json:"version"`
	SessionAttributes map[string]interface{} `json:"sessionAttributes,omitempty"`
	Response          EchoResponseBody       `json:"response"`
}

type EchoResponseBody struct {
	OutputSpeech     *EchoOutput   `json:"outputSpeech,omitempty"`
	Card             *EchoCard     `json:"card,omitempty"`
	Reprompt         *EchoReprompt `json:"reprompt,omitempty"` // Pointer so it's dropped if empty in JSON response.
	ShouldEndSession bool          `json:"shouldEndSession"`
}

type EchoReprompt struct {
	OutputSpeech EchoOutput `json:"outputSpeech,omitempty"`
}

type EchoOutput struct {
	Type string `json:"type,omitempty"`
	Text string `json:"text,omitempty"`
	SSML string `json:"ssml,omitempty"`
}

type EchoCard struct {
	Type    string `json:"type"`
	Title   string `json:"title"`
	Content string `json:"content"`
	Text    string `json:"text"`
	Image   struct {
		SmallImageURL string `json:"smallImageUrl"`
		LargeImageURL string `json:"largeImageUrl"`
	} `json:"image"`
}

// FUNCTIONS //////////////////////////////////////////////////////////////////

func NewResponse() *EchoResponse {
	er := &EchoResponse{
		Version: "1.0",
		Response: EchoResponseBody{
			ShouldEndSession: false,
		},
		SessionAttributes: make(map[string]interface{}),
	}
	return er
}

func (er *EchoResponse) OutputText(text string) *EchoResponse {
	er.Response.OutputSpeech = &EchoOutput{
		Type: "PlainText",
		Text: text,
	}
	return er
}

func (er *EchoResponse) OutputSSML(text string) *EchoResponse {
	er.Response.OutputSpeech = &EchoOutput{
		Type: "SSML",
		SSML: text,
	}
	return er
}

func (er *EchoResponse) OutputSSML2(name string, data SSMLData) *EchoResponse {
	return er.OutputSSML(GetSSML(name, data))
}

func (er *EchoResponse) Card(title string, content string) *EchoResponse {
	return er.SimpleCard(title, content)
}

func (er *EchoResponse) SimpleCard(title string, content string) *EchoResponse {
	er.Response.Card = &EchoCard{
		Type:    "Simple",
		Title:   title,
		Content: content,
	}
	return er
}

func (er *EchoResponse) StandardCard(title string, content string, smallImg string, largeImg string) *EchoResponse {
	er.Response.Card = &EchoCard{
		Type:    "Standard",
		Title:   title,
		Content: content,
	}

	if smallImg != "" {
		er.Response.Card.Image.SmallImageURL = smallImg
	}

	if largeImg != "" {
		er.Response.Card.Image.LargeImageURL = largeImg
	}
	return er
}

func (er *EchoResponse) LinkAccountCard() *EchoResponse {
	er.Response.Card = &EchoCard{
		Type: "LinkAccount",
	}
	return er
}

func (er *EchoResponse) RepromptText(text string) *EchoResponse {
	er.Response.Reprompt = &EchoReprompt{
		OutputSpeech: EchoOutput{
			Type: "PlainText",
			Text: text,
		},
	}
	return er
}

func (er *EchoResponse) RepromptSSML(text string) *EchoResponse {
	er.Response.Reprompt = &EchoReprompt{
		OutputSpeech: EchoOutput{
			Type: "SSML",
			Text: text,
		},
	}
	return er
}
func (er *EchoResponse) RepromptSSML2(name string, data SSMLData) *EchoResponse {
	return er.RepromptSSML(GetSSML(name, data))
}

func (er *EchoResponse) EndSession() *EchoResponse {
	er.Response.ShouldEndSession = true
	return er
}

func (er *EchoResponse) Data() []bytes {
	jsonStr, err := json.Marshal(&er)
	if err != nil {
		return nil
	}
	return jsonStr
}

func (er *EchoResponse) String() string {
	return string(er.Data())
}

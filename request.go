package amzn

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"net/http"
	"time"
)

// ParseRequest parses a http Request and creates an EchoRequest on success
func ParseRequest(r *http.Request) (*EchoRequest, error) {
	body, err := ioutil.ReadAll(r.Body)
	if err != nil {
		return nil, err
	}

	req := EchoRequest{}
	err = json.Unmarshal(body, &req)
	if err != nil {
		return nil, err
	}

	return &req, nil
}

type EchoRequest struct {
	Version string          `json:"version"`
	Session EchoSession     `json:"session"`
	Request EchoRequestBody `json:"request"`
}

type EchoSession struct {
	New         bool   `json:"new"`
	SessionID   string `json:"sessionId"`
	Application struct {
		ApplicationID string `json:"applicationId"`
	} `json:"application"`
	Attributes map[string]interface{} `json:"attributes"`
	User       struct {
		UserID      string `json:"userId"`
		AccessToken string `json:"accessToken,omitempty"`
	} `json:"user"`
}

type EchoRequestBody struct {
	Type      string     `json:"type"`
	RequestID string     `json:"requestId"`
	Timestamp string     `json:"timestamp"`
	Intent    EchoIntent `json:"intent,omitempty"`
	Reason    string     `json:"reason,omitempty"`
}

type EchoIntent struct {
	Name  string              `json:"name"`
	Slots map[string]EchoSlot `json:"slots"`
}

type EchoSlot struct {
	Name  string `json:"name"`
	Value string `json:"value"`
}

// FUNCTIONS //////////////////////////////////////////////////////////////////
func (er *EchoRequest) VerifyTimestamp() bool {
	reqTimestamp, _ := time.Parse("2006-01-02T15:04:05Z", er.Request.Timestamp)
	return time.Since(reqTimestamp) < time.Duration(150)*time.Second
}

func (er *EchoRequest) VerifyAppID(appID string) bool {
	return (er.Session.Application.ApplicationID == appID)
}

func (er *EchoRequest) GetSessionID() string {
	return er.Session.SessionID
}

func (er *EchoRequest) GetUserID() string {
	return er.Session.User.UserID
}

func (er *EchoRequest) GetRequestType() string {
	return er.Request.Type
}

func (er *EchoRequest) GetIntentName() string {
	if er.GetRequestType() == "IntentRequest" {
		return er.Request.Intent.Name
	}

	return er.GetRequestType()
}

func (er *EchoRequest) GetSlotValue(slotName string) (string, error) {
	if _, ok := er.Request.Intent.Slots[slotName]; ok {
		return er.Request.Intent.Slots[slotName].Value, nil
	}
	return "", errors.New("Slot name not found.")
}

func (er *EchoRequest) AllSlots() map[string]EchoSlot {
	return er.Request.Intent.Slots
}

func (er *EchoRequest) Data() []bytes {
	jsonStr, err := json.Marshal(&er)
	if err != nil {
		return nil
	}
	return jsonStr
}
func (er *EchoRequest) String() string {
	return string(er.Data())
}

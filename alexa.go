package amzn

import (
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"strings"

	log "github.com/sirupsen/logrus"
)

// SSMLDirectory is the directory where we can find the ssml templates
// to be used in responses
var SSMLDirectory string

type Intent func(req *EchoRequest, resp *EchoResponse)
type Exit func(req *EchoRequest)
type ReqErr func(log string)

// Alexa object represents everything coming in from an Alexa device
type Alexa struct {
	appID         string
	onLaunch      []Intent
	onExit        Exit
	intents       map[string][]Intent
	faultyRequest ReqErr
}

// NewAlexa returns a simple Alexa Object
// specify the http port
func NewAlexa() *Alexa {
	return &Alexa{
		appID:   "",
		intents: make(map[string][]Intent),
	}
}

// OnIntent takes funtiions to be called on invoking a certain intent
func (ax *Alexa) OnIntent(intent string, cb ...Intent) {
	ax.intents[intent] = cb
}

// OnLaunch takes functions to be called on skill launch
func (ax *Alexa) OnLaunch(cb ...Intent) {
	ax.onLaunch = cb
}

// OnExit takes functions to be called if the skill was exited for unknown reason
func (ax *Alexa) OnExit(cb Exit) {
	ax.onExit = cb
}

// OnRequestError takes a function to be called if a faulty request comes in
// mostly used for logging purposes
func (ax *Alexa) OnRequestError(cb ReqErr) {
	ax.faultyRequest = cb
}

// Run starts the service on a certain port
func (ax *Alexa) Run(port int) {
	http.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		req, err := ax.getValidEchoRequest(r)
		if req != nil {
			resp := NewResponse()
			if req.Request.Type == "LaunchRequest" {
				log.Debug("launch")
				for i := 0; i < len(ax.onLaunch); i++ {
					ax.onLaunch[i](req, resp)
				}
				w.Write(resp.Data())
			} else if req.Request.Type == "SessionEndedRequest" {
				log.Debug("exit")
				ax.onExit(req)
			} else if intents, exists := ax.intents[req.Request.Intent.Name]; exists {
				log.Debug("intent: ", req.Request.Intent.Name)
				for i := 0; i < len(intents); i++ {
					intents[i](req, resp)
				}
				w.Write(resp.Data())
			}

			w.WriteHeader(200)

		} else {
			ax.faultyRequest(err)
			w.WriteHeader(400)
		}
	})

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}

func (ax *Alexa) getValidEchoRequest(r *http.Request) (*EchoRequest, string) {
	var req *EchoRequest

	// verify the http request
	if ok, err := verifyRequest(r); !ok {
		return nil, err.Error()
	}

	// try to parse into EchoRequest
	req, err = ParseRequest(r)
	if req == nil {
		return nil, err.Error()
	}

	// looking for valid timestamps and app id
	if ok := req.VerifyTimestamp(); !ok {
		return nil, "invalid timestamp"
	}

	if ax.appID != "" && req.VerifyAppID() {
		return nil, "invalid app id"
	}

	return req, ""
}

func verifyRequest(r *http.Request) (bool, error) {
	verifyURL := r.Header.Get("SignatureCertChainUrl")
	uri, err := url.Parse(verifyURL)
	if err != nil {
		return false, err
	}
	uri = uri.ResolveReference(uri)

	if uri.Scheme != "https" {
		return false, errors.New("protocol error")
	}
	if strings.ToLower(uri.Host) != "s3.amazonaws.com" {
		return false, errors.New("host error")
	}
	if strings.HasPrefix(uri.Path, "/echo.api/") {
		return false, errors.New("path error")
	}
	if uri.Port() != "443" && uri.Port() != "" {
		return false, errors.New("port error")
	}

	return true, nil
}
